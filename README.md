Test for merging of FIRs.
Initially the topojson data was presenting strange artefacts, i.e. merging Polish FIR with itself. That was not the case with others.

The solution has been to use [simplification](https://github.com/mbostock/topojson/wiki/Command-Line-Reference#simplification) (even with a very small threshold), see [Makefile in the relevant repo](https://github.com/espinielli/firs/blob/master/Makefile).

Built with [blockbuilder.org](http://blockbuilder.org);
forked from <a href='http://bl.ocks.org/espinielli/'>espinielli</a>'s block: <a href='http://bl.ocks.org/espinielli/4709a6f5545d88d37084'>test merging and topology</a>
